const cjs = require('comfy.js');
const u = require('unirest');
const fs = require('fs');
const ini = require('ini');
const jp = require('jsonpath');
const path =require('path');
const config = ini.parse(fs.readFileSync(path.resolve(__dirname,'./config.ini'), 'utf-8'));
// Apis
const dadjokes = 'https://icanhazdadjoke.com/';
const cn = 'https://api.icndb.com/jokes/random?exclude=[explicit]';
const weather = `https://api.openweathermap.org/data/2.5/onecall?lat=${config.weather.lat}&lon=${config.weather.lon}&exclude=minutely,hourly,daily&appid=${config.weather.weatherAPI}&units=${config.weather.units}`;
const catfacts = `https://cat-fact.herokuapp.com/facts`; //remember to add math random function
const badadvice = `https://api.adviceslip.com/advice`;
const pokeman = `https://pokeapi.co/api/v2`;
const version = 2.0;

// Caps Function
const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}
//Random Num:
function randomNum(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

// Login to twitch as user as defined in config
cjs.Init(config.twitch.user, config.twitch.key);
console.log(`Logged into twitch chat as: ${config.twitch.user}`);

cjs.onCommand = (user, command, message, flags, self, extra) => {

    switch (command) {
        case 'site':
            cjs.Say(config.misc.site);
            break;

        case 'lurk':
            cjs.Say(`${user} is lurking in the shadows waiting for their time to strike.`);
            break;

        case 'shoutout':
            if ((flags.broadcaster || flags.mod) && message !== undefined) {
                cjs.Say(`Hey go give ${message} a look, they seem pretty cool.`);
                cjs.Say(`twitch.tv/${message}`);
            }
            break;

        case 'weather':
            u.post(weather).end(function (result) {
                let currentWeather = result.body;
                currentWeather = jp.query(currentWeather, '$.current.weather.0.main');
                cjs.Say("Zer0's Current weather is: " + currentWeather.toString() + ", With a temperature of: " + jp.query(result.body, '$.current.temp') + "°C ItsBoshyTime");
            })
            break;

        case 'dadjoke':
            u.get(dadjokes)
                .header("Accept", "text/plain")
                .header("User-Agent", `Z-Bot Version ${version} (https://gitlab.com/Zer0Xeon/z-bot)`)
                .end(function (result) {
                    cjs.Say(result.body);
                })
            break;

        case 'chucknorris':
            u.get(cn).end(function (result) {
                let id = jp.query(result.body, '$.value.id');
                let quote = jp.query(result.body, '$.value.joke');
                cjs.Say(`#${id}: ${quote}`);
            })
            break;

        case 'vape':
            cjs.Say(`${config.misc.mod}`);
            break;

        case 'juice':
            cjs.Say(`${config.misc.juice}`);
            break;

        case 'site':
            cjs.Say(`I'm over here at ${config.misc.site}`);
            break;

        case 'time':
            if (!message) {
                u.get('http://worldclockapi.com/api/json/gmt/now')
                    .end(function (result) {
                        let td = result.body.currentDateTime;
                        console.log(td);
                        cjs.Say(`The time and date for Zer0 is ${td}`);
                        console.log(result.body)
                    })
            } else {
                let tz = message
                u.get(`http://worldclockapi.com/api/json/${tz}/now`)
                    .end(function (result) {
                        let td = result.body.currentDateTime;
                        console.log(td);
                        cjs.Say(`Zer0's time and date in ${tz} is ${td}`);
                    })
            }
            break;
        case 'pkmn':
            if (!message) {
                cjs.Say('MISSING NO!');
            } else {
                let idno = message;
                u.get(`https://pokeapi.co/api/v2/pokemon/${idno}`)
                    .end(function (result) {
                        let td = result.body.name;
                        let pkname = capitalize(td);
                        console.log(pkname);
                        cjs.Say(`That Pokemon is ${pkname}!`);
                    })
            }
            break;
        case 'catfact':
            u.get(`https://cat-fact.herokuapp.com/facts`)
                .end(function (result) {
                    let num = randomNum(223);
                    let fact = jp.query(result.body, `$.all.${num}.text`).toString();
                   //console.log(fact)
                    cjs.Say(fact);
                })
            break;
        case 'advice':
            u.get(`https://api.adviceslip.com/advice`)
            .header('content-type', 'application/json')
                .end(function (result) {
                   let jsondata = JSON.parse(result.body);
                    let idno = jp.query(jsondata, '$.slip.id').toString();
                    let text = jp.query(jsondata, '$.slip.advice').toString();
                    let advice = `[${idno}]: ${text}`;
                    cjs.Say(advice);
                })
                break;
        case 'help':
            cjs.Say(config.misc.help);
    }
}

cjs.onChat = (user, message) => {
    console.log(`${user}: ${message}`);
}